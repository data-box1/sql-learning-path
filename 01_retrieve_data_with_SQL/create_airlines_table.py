# https://pynative.com/python-sqlite/
# https://www.adamsmith.haus/python/answers/how-to-insert-the-contents-of-a-csv-file-into-an-sqlite3-database-in-python
import sqlite3
import pandas as pd
try:
    conn = sqlite3.connect('SQLite_Python.db')
    c = conn.cursor()
    print("Database created and Successfully Connected to SQLite")

    db_file = "./archive/airlines.csv"
    #IATA_CODE,AIRLINE

    df = pd.read_csv(db_file)
    col_name_type=''
    for column in df.columns:
        col_name_type += ' '+column.lower() + ' TEXT'

    print(col_name_type)
    sql_command = "CREATE TABLE if not exists  airlines ("+col_name_type+")"
    c.execute(sql_command)
    # Write the data to a sqlite db table
    df.to_sql('airlines', conn, if_exists='replace', index=False)

    # Run select sql query
    c.execute('select * from airlines')
    
    # Fetch all records
    # as list of tuples
    records = c.fetchall()
    
    # Display result 
    for row in records:
        # show row
        print(row)

    c.close()

except sqlite3.Error as error:
    print("Error while connecting to sqlite", error)
finally:
    if conn:
        conn.close()
        print("The SQLite connection is closed")
