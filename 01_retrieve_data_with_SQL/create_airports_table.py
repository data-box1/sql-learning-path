# https://pynative.com/python-sqlite/
# https://www.adamsmith.haus/python/answers/how-to-insert-the-contents-of-a-csv-file-into-an-sqlite3-database-in-python
import sqlite3
import pandas as pd
try:
    #https://www.geeksforgeeks.org/python-sqlite-working-with-date-and-datetime/
    conn = sqlite3.connect('SQLite_Python.db',
                             detect_types=sqlite3.PARSE_DECLTYPES |
                             sqlite3.PARSE_COLNAMES)

    c = conn.cursor()
    print("Database created and Successfully Connected to SQLite")

    db_file = "./archive/airports.csv"
    #IATA_CODE,AIRPORT,CITY,STATE,COUNTRY,LATITUDE,LONGITUDE

    df = pd.read_csv(db_file)
    col_name_type=''
    for column in df.columns:
        if(column=='LATITUDE' or column=='LONGITUDE'):
            col_name_type += ' '+column.lower() + ' REAL'
        else:
            col_name_type += ' '+ column.lower() + ' TEXT'
    '''
    #https://www.geeksforgeeks.org/sqlite-datatypes-and-its-corresponding-python-types/

    NULL: None
    INTEGER: int
    REAL: float
    TEXT: str
    BLOB: bytes
    '''
    print(col_name_type)
    sql_command = "CREATE TABLE if not exists  airports ("+col_name_type+")"
    c.execute(sql_command)
    # Write the data to a sqlite db table
    df.to_sql('airports', conn, if_exists='replace', index=False)

    # Run select sql query
    c.execute('select * from airports')
    
    # Fetch all records
    # as list of tuples
    records = c.fetchall()
    
    # Display result 
    for row in records:
        # show row
        print(row)
    c.close()

except sqlite3.Error as error:
    print("Error while connecting to sqlite", error)
finally:
    if conn:
        conn.close()
        print("The SQLite connection is closed")
