# https://pynative.com/python-sqlite/
# https://www.adamsmith.haus/python/answers/how-to-insert-the-contents-of-a-csv-file-into-an-sqlite3-database-in-python
import sqlite3
import pandas as pd
import datetime
import numpy as np
try:
    conn = sqlite3.connect('SQLite_Python.db')
    c = conn.cursor()
    print("Database created and Successfully Connected to SQLite")

    db_file = "./archive/flights.csv"
    #YEAR,MONTH,DAY,DAY_OF_WEEK,AIRLINE,FLIGHT_NUMBER,TAIL_NUMBER,ORIGIN_AIRPORT,DESTINATION_AIRPORT,SCHEDULED_DEPARTURE,
    #DEPARTURE_TIME,DEPARTURE_DELAY,TAXI_OUT,WHEELS_OFF,SCHEDULED_TIME,ELAPSED_TIME,AIR_TIME,DISTANCE,WHEELS_ON,TAXI_IN,
    #SCHEDULED_ARRIVAL,ARRIVAL_TIME,ARRIVAL_DELAY,DIVERTED,CANCELLED,CANCELLATION_REASON,AIR_SYSTEM_DELAY,SECURITY_DELAY,AIRLINE_DELAY,LATE_AIRCRAFT_DELAY,
    #WEATHER_DELAY
    
    # Parse datetime columns
    df = pd.read_csv(db_file,parse_dates=[9,10], date_parser=lambda x: pd.to_datetime(x, format='%H%M%S').time)
    # Handle Not a Date removing all those rows
    df = df[df['DEPARTURE_TIME'].notna()]

    # parse date row
    date = df.apply(lambda x: datetime.date(int(x['YEAR']), x['MONTH'], x['DAY']),axis=1)
    # parse Date and time for SCHEDULED departures
    sch_dep=pd.to_datetime(date.astype(str)+' '+df['SCHEDULED_DEPARTURE'].astype(str), format='%Y-%m-%d %H:%M:%S')
    
    '''
    # Save a column debug
    f = open("SCHEDULED_DEPARTURE.dat", "w")
    for ind in df.index:
        f.write('%s\n'%df['DEPARTURE_TIME'][ind])
     #if ( == np.datetime64('NaT')): print("%d:  %s"%(ind,df['DEPARTURE_TIME'][ind]))
    f.close()
    '''
    # parse Date and time for departures
    dep=pd.to_datetime(date.astype(str)+' '+df['DEPARTURE_TIME'].astype(str), format='%Y-%m-%d %H:%M:%S')#, errors='coerce'

    # rm raw data
    df = df.drop(columns=['YEAR', 'MONTH', 'DAY','SCHEDULED_DEPARTURE','DEPARTURE_TIME'])
    # Insert post processed columns in 1st, 2nd place
    df.insert(0, 'SCHEDULED_DEPARTURE', sch_dep)
    df.insert(1, 'DEPARTURE_TIME', dep)
    print(df.head())
    
    col_name_type=''
    for column in df.columns:
        if(column=='SCHEDULED_DEPARTURE' or column=='DEPARTURE_TIME'):
            col_name_type += ' '+column.lower() + ' TIMESTAMP'
        else:
            col_name_type += ' '+ column.lower() + ' TEXT'

    createTable = "CREATE TABLE if not exists  flights ("+col_name_type+")"
    c.execute(createTable)
    # Write the data to a sqlite db table
    df.to_sql('flights', conn, if_exists='replace', index=False)

    # Run select sql query
    c.execute('select * from flights limit 10;')
    
    # Fetch all records
    # as list of tuples
    records = c.fetchall()
    
    # Display result 
    for row in records:
        # show row
        print(row)

    c.close()

except sqlite3.Error as error:
    print("Error while connecting to sqlite", error)
finally:
    if conn:
        conn.close()
        print("The SQLite connection is closed")
