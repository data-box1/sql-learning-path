## Tutorial: Retrieve data with SQL 

Create sqlitle db for following the [Tutorial on how to retrieve data with SQL](https://www.mit.edu/~amidi/teaching/data-science-tools/tutorial/queries-with-sql/).

### Setup 

1. Downnload the Department of Transportation [dataset](https://www.kaggle.com/usdot/flight-delays/download) and decompress

2. Install sqlitle

3. Create de tables from running python create_*.py files in the command line. This will creates a SQLite_Python.db file that stores the created tables.

4. Open the database with ```sqlite3 ./SQLite_Python.db```

5. Follow the tutorial